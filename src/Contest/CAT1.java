package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...
    // Class CAT1 merupakan subclass dari TiketKonser.
    public CAT1(String nama,double harga) {
        // Konstruktor untuk class CAT1, menginisialisasi nama tiket dan harga tiket.
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}