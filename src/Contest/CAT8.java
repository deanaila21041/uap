package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...
    // Class CAT8 merupakan subclass dari TiketKonser.
    public CAT8(String nama,double harga) {
        // Konstruktor untuk class CAT8, menginisialisasi nama tiket dan harga tiket.
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}