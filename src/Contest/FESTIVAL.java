package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...
    // Class FESTIVAL merupakan subclass dari TiketKonser.
    public FESTIVAL(String nama,double harga) {
        // Konstruktor untuk class FESTIVAL, menginisialisasi nama tiket dan harga tiket.
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}