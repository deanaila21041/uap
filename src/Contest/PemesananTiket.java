package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...
    // Menyimpan nama pemesan dan tiket yang dipesan.
    static String namaPemesan; 
    static TiketKonser dipesan;   
    
    // Melakukan proses pemesanan tiket.
    public static void run() throws InvalidInputException {
        Scanner sc = new Scanner(System.in);

        // Meminta input nama pemesan.
        System.out.print("Masukkan nama pemesan : ");
        namaPemesan = sc.nextLine();
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter");
        }

        // Menampilkan pilihan jenis tiket.
        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. VVIP (UNLIMITED EXPERIENCE)");
        System.out.println("Masukkan pilihan :");
        int pilihan = sc.nextInt();
        if (pilihan < 1 || pilihan > 5) {
            throw new InvalidInputException("Pilihan tiket harus antara 1 hingga 5");
        }

        // Membuat objek tiket berdasarkan pilihan pengguna.
        switch (pilihan) {
            case 1:
                dipesan = new CAT8("CAT8", 800000);
                break;
            case 2:
                dipesan = new CAT1("CAT1", 3500000);
                break;
            case 3:
                dipesan = new FESTIVAL("FESTIVAL", 5000000);
                break;
            case 4:
                dipesan = new VIP("VIP", 5700000);
                break;
            case 5:
                dipesan = new VVIP("VVIP (UNLIMITED EXPERIENCE)", 9900000);
                break;
            default:
                break;
        }
    }
}