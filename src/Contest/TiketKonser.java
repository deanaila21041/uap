package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    // Menyimpan nama dan harga tiket konser.
     String namaTiket;
     double hargaTiket;

    @Override
    public double getHarga() {
        // Mengembalikan harga tiket konser.
        return this.hargaTiket;
    }
}